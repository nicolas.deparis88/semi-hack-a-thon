FROM python:3.7-alpine

RUN pip3 install flask
RUN pip3 install flask_restplus

ADD server.py .
CMD python3 server.py
