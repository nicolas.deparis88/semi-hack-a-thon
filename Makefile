build:
	docker build -t baseapi .

deploy:
	docker stack deploy -c docker-compose.yml baseapi

update:
	docker service update --force baseapi
